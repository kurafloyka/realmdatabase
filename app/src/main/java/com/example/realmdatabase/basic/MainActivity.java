package com.example.realmdatabase.basic;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;

import com.example.realmdatabase.R;

import io.realm.Realm;
import io.realm.RealmResults;

public class MainActivity extends AppCompatActivity {
    Realm realm = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        defineRealm();
        addTable();
        showData();
    }


    public void defineRealm() {


        try {
            realm = Realm.getDefaultInstance(); // opens "myrealm.realm"
        } catch (Exception e) {
        }

    }

    public void addTable() {
        realm.beginTransaction();
        PersonTables personTables = realm.createObject(PersonTables.class);
        personTables.setName("FARUK");
        personTables.setSurname("AKYOL");
        personTables.setAge(28);
        personTables.setIncome(7000);

        realm.commitTransaction();

    }


    public void showData() {

        realm.beginTransaction();
        RealmResults<PersonTables> results = realm.where(PersonTables.class).findAll();
        for (PersonTables p : results) {
            Log.i("output : ", p.toString());
        }
        realm.commitTransaction();
    }
}
