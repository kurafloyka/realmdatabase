package com.example.realmdatabase.basic;

import io.realm.RealmObject;
import io.realm.annotations.RealmClass;

@RealmClass
public class PersonTables extends RealmObject {
    private String name, surname;
    private Integer income, age;

    @Override
    public String toString() {
        return "PersonTables{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", income=" + income +
                ", age=" + age +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Integer getIncome() {
        return income;
    }

    public void setIncome(Integer income) {
        this.income = income;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }
}
