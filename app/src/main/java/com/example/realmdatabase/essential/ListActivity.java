package com.example.realmdatabase.essential;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;


import com.example.realmdatabase.R;

import io.realm.Realm;
import io.realm.RealmResults;

public class ListActivity extends AppCompatActivity {

    Realm realm = null;
    EditText nickName, userName, password;
    RadioGroup sex;
    Button submitButton, checkButton, updateButton;
    String userNameValue, nickNameValue, passwordValue, sexValue;
    ListView listViewPersonel;
    Integer pos = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        defineView();
        defineRealm();
        addInfo();
        checkDB();
        listPersonel();
        getPositionOnList();

    }


    public void defineView() {

        nickName = findViewById(R.id.editTextNickName);
        userName = findViewById(R.id.editTextUserName);
        password = findViewById(R.id.password);
        submitButton = findViewById(R.id.submitButton);
        sex = findViewById(R.id.sexRadioGroup);
        checkButton = findViewById(R.id.check);
        listViewPersonel = findViewById(R.id.listViewPersonel);
        updateButton = findViewById(R.id.update);


    }

    public void checkDB() {

        checkButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkTables();
            }
        });
    }

    public void defineRealm() {


        try {
            realm = Realm.getDefaultInstance();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }


    }

    public void addInfo() {


        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userNameValue = userName.getText().toString();
                nickNameValue = nickName.getText().toString();
                passwordValue = password.getText().toString();
                Integer id = sex.getCheckedRadioButtonId();
                RadioButton radioButtonSex = findViewById(id);
                sexValue = radioButtonSex.getText().toString();
                addRealm();
                userName.setText("");
                nickName.setText("");
                password.setText("");

            }
        });

        updateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                RealmResults<PersonInfo> personInfos = realm.where(PersonInfo.class).findAll();
                final PersonInfo personInfo = personInfos.get(pos);

                userNameValue = userName.getText().toString();
                nickNameValue = nickName.getText().toString();
                passwordValue = password.getText().toString();
                Integer id = sex.getCheckedRadioButtonId();
                RadioButton radioButtonSex = findViewById(id);
                sexValue = radioButtonSex.getText().toString();

                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        personInfo.setSex(sexValue);
                        personInfo.setPassword(passwordValue);
                        personInfo.setUserName(userNameValue);
                        personInfo.setNickName(nickNameValue);

                    }
                });
                listPersonel();



            }
        });

    }


    public void addRealm() {


        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {

                PersonInfo personInfo = realm.createObject(PersonInfo.class);
                personInfo.setNickName(nickNameValue);
                personInfo.setUserName(userNameValue);
                personInfo.setPassword(passwordValue);
                personInfo.setSex(sexValue);

            }
        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {

                Toast.makeText(getApplicationContext(), "SUCCESS", Toast.LENGTH_LONG).show();

            }
        }, new Realm.Transaction.OnError() {
            @Override
            public void onError(Throwable error) {
                Toast.makeText(getApplicationContext(), "FAILED", Toast.LENGTH_LONG).show();
            }
        });
    }


    public void checkTables() {
        RealmResults<PersonInfo> personInfos = realm.where(PersonInfo.class).findAll();

        for (PersonInfo p : personInfos) {


            Log.i("Output :", p.toString());
        }
    }

    public void listPersonel() {
        RealmResults<PersonInfo> personInfos = realm.where(PersonInfo.class).findAll();


        if (personInfos.size() > 0) {

            Adapter adapter = new Adapter(personInfos, getApplicationContext());
            listViewPersonel.setAdapter(adapter);
        }
    }

    public void getPositionOnList() {
        listViewPersonel.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                RealmResults<PersonInfo> personInfos = realm.where(PersonInfo.class).findAll();

                openAlertDialog(position);

                nickName.setText(personInfos.get(position).getNickName());
                userName.setText(personInfos.get(position).getUserName());
                password.setText(personInfos.get(position).getPassword());


                if (personInfos.get(position).getSex().equals("Male")) {

                    ((RadioButton) sex.getChildAt(0)).setChecked(true);
                } else {


                    ((RadioButton) sex.getChildAt(1)).setChecked(true);
                }

                pos = position;

            }
        });
    }

    public void deletePersonel(final int position) {
        Log.i("CheckId : ", " " + position);

        final RealmResults<PersonInfo> personInfos = realm.where(PersonInfo.class).findAll();

        Log.i("Size", " " + personInfos.size() + " " + personInfos.get(position).getNickName());

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {

                PersonInfo personInfo = personInfos.get(position);
                personInfo.deleteFromRealm();
                listPersonel();
                userName.setText("");
                nickName.setText("");
                password.setText("");

            }
        });
    }


    public void openAlertDialog(final int position) {

        LayoutInflater inflater = getLayoutInflater();
        View view = inflater.inflate(R.layout.alertlayout, null);

        Button yesButton = view.findViewById(R.id.yesButton);
        Button noButton = view.findViewById(R.id.noButton);
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setView(view);
        alert.setCancelable(false);
        final AlertDialog dialog = alert.create();

        yesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deletePersonel(position);
                dialog.cancel();
            }
        });

        noButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });
        dialog.show();

    }

}
