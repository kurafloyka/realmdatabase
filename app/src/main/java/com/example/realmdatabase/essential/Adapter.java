package com.example.realmdatabase.essential;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.realmdatabase.R;

import java.util.List;

public class Adapter extends BaseAdapter {

    List<PersonInfo> list;
    Context context;

    public Adapter(List<PersonInfo> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        convertView = LayoutInflater.from(context).inflate(R.layout.personellayout, parent, false);

        TextView nickName = convertView.findViewById(R.id.editTextNickName);
        TextView userName = convertView.findViewById(R.id.editTextUserName);
        TextView password = convertView.findViewById(R.id.editTextPassword);
        TextView sex = convertView.findViewById(R.id.editTextSex);

        nickName.setText(list.get(position).getNickName());
        userName.setText(list.get(position).getUserName());
        password.setText(list.get(position).getPassword());
        sex.setText(list.get(position).getSex());
        return convertView;
    }
}
